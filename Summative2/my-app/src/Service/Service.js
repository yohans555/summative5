import axios from 'axios'

const api_url_get = 'http://localhost:8080/';

class Service {
    
    getData(target){
        const url_target = api_url_get+"get/"+target
        return axios.get(url_target);
    }
}

export default new Service()