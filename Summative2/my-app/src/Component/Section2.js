import React from "react";
class Section2 extends React.Component {
  render() {
    return (
      <div className="section2">
        <h2>Blog : Lorem Ipsum sodales</h2>
        <p>
          Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum
          Dolor Sit Amet.
        </p>
        <img src="images/dolphins.jpg" />
        <ul>
          <li>Lorem</li>
          <li>Lorem</li>
          <li>Lorem</li>
          <li>Lorem</li>
          <li>Lorem</li>
        </ul>


        <div id="section1">
          <ul>
            <li>
              <a href="#">
                <img src="images/penguin.jpg" />
              </a>
              <h4>
                <a>Lorem Ipsum dolor sit amet</a>
              </h4>
              <p>Lorem Ipsum dolor sit amet</p>
            </li>
            <li>
              <a>
                <img src="images/snake.jpg" />
              </a>
              <h4>
                <a>Lorem Ipsum dolor sit amet</a>
              </h4>
              <p>Lorem Ipsum dolor sit amet</p>
            </li>
          </ul>
        </div>
        

        <div id="section2">
          <ul>
            <li>
              <img src="images/butterfly-2.jpg" alt="" />
              <h4>Lorem Ipsum Dolor sit amet</h4>
              <p>Lorem Ipsum Dolor sit amet Lorem Ipsum Dolor sit amet</p>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
export default Section2;
