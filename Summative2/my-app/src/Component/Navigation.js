import React from 'react';


class Navigation extends React.Component {


    render() {
        return (
            <>
                <ul id="navigation">
					<li id="link1">Home</li>
					<li id="link2">The Zoo</li>
					<li id="link3">Visitors Info</li>
					<li id="link4">Tickets</li>
					<li id="link5">Events</li>
					<li id="link6">Gallery</li>
					<li id="link7">Contact Us</li>
				</ul>
            </>
        )
    }
}

export default Navigation;