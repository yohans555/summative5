import React from 'react';
import Service from '../Service/Service'

class ImageGalery extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            datas:[]
        }
    }

    componentDidMount(){
        Service.getData("image").then((response)=>{
            this.setState({datas:response.data})  
        })
    }

    render() {
        return (
            <>
            <ul>
                {/* <li style={{marginLeft:0}}><img src='images/penguin.jpg'></img> Lorem Ipsum</li>
                <li><img src='images/snake.jpg'></img> Lorem Ipsum</li>
                <li><img src='images/penguin.jpg'></img> Lorem Ipsum</li>
                <li><img src='images/turtle.jpg'></img> Lorem Ipsum</li>
                <li><img src='images/owl.jpg'></img> Lorem Ipsum</li>
                <li><img src='images/elephant.jpg'></img> Lorem Ipsum</li>
                <li><img src='images/gorilla.jpg'></img> Lorem Ipsum</li>
                <li><img src='images/butterfly.jpg'></img> Lorem Ipsum</li> */}
                {
                    this.state.datas.map(response =>(
                        <li  key={response.id}>
                            <img src={response.img}></img> {response.title}
                        </li>
                    ))
                }
            </ul>
            </>
        )
    }
}

export default ImageGalery;