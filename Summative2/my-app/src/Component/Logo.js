import React from 'react';

class Logo extends React.Component {

    render() {
        return (
            <>
               <a id="logo"><img src="images/logo.jpg" /></a>
            </>
        )
    }
}

export default Logo;