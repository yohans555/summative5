import React from 'react';
import Service from '../Service/Service'
class Section1 extends React.Component {
	constructor(props){
        super(props)
        this.state = {
            datas:[]
        }
    }

    componentDidMount(){
        Service.getData("section").then((response)=>{
            this.setState({datas:response.data})  
        })
    }

    render() {
        return (
            <div className='section1'>
                <h2>Events</h2>
					<ul id="article">
                       {
						   this.state.datas.map(response =>(
							<li  key={response.id}>
								<a href="#"><span>{response.title}</span></a>
								<p>{response.body}</p>
							</li>
						))
					   }
					</ul>
            </div>
        )
    }
}
export default Section1;