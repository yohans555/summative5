import React from 'react';

class First extends React.Component {
    render() {
        return (
            <>
            <ul>
					<li >
						<h2><a >Live</a></h2>
						<span>Have fun in your visit</span>
					</li>
					<li>
						<h2><a >Love</a></h2>
						<span>Donate for the animals</span>
					</li>
					<li>
						<h2><a>Learn</a></h2>
						<span>Get to know the animals</span>
					</li>
				</ul>
            </>
        )
    }
}

export default First;