import IndexPage from './Main/Index/IndexPage';
import Footer from './Template/Footer';
import Header from './Template/Header';

function App() {
  return (
    <>
    <Header />
    <IndexPage />
    <Footer/>
    </>
  );
}

export default App;
