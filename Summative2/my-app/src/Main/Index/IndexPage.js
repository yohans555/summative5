import React from "react";
import Section1 from "../../Component/Section1";
import Section2 from "../../Component/Section2";
import Section3 from "../../Component/Section3";
import Featured from "../../Component/Featured";

class IndexPage extends React.Component {
  render() {
    return (
      <div id="content">
        <Featured />
        <Section1 />
        <Section2 />
        <Section3 />
      </div>
    );
  }
}

export default IndexPage;
