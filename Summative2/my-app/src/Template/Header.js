import React from 'react';
import Logo from '../Component/Logo'
import First from '../Component/first'
import Navigation from '../Component/Navigation'
import SpecialEvent from '../Component/SpecialEvent'
import Headerimg from '../Component/Headerimg'

class Header extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            datas:[]
        }
    }

    render() {
        return (
            <div id='page'>
                <div id='header'>
                    <Logo />
                    <First />
                    <a>Buy tickets / Check Events</a>
                    <Navigation/>
                    <Headerimg/>
                    <SpecialEvent/>
                </div>
            </div>
        )
    }
}

export default Header;