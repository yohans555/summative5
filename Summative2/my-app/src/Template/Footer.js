import React from 'react';
import Service from '../Service/Service'

class Footer extends React.Component {
    render() {
        return (
            <div id='footer'>
                <div>
                    <img className='logo' src='images/animal-kingdom.jpg'></img>

                    <div>
                        <p>Lorem Ipsum DOlor sit Amet</p>
                        <p>Lorem Ipsum DOlor sit Amet</p>
                        <p>Lorem Ipsum DOlor sit Amet</p>
                    </div>
                    <ul className='navigation'>
                        <li>Lorem Ipsum</li>
                        <li>Lorem Ipsum</li>
                        <li>Lorem Ipsum</li>
                        <li>Lorem Ipsum</li>
                    </ul>

                    <ul>
                        <li>Lorem Ipsum</li>
                        <li>Lorem Ipsum</li>
                        <li>Lorem Ipsum</li>
                    </ul>
                <p>Lorem Ipsum Dolor sit amet</p>
                </div>
            </div>
        )
    }
}

export default Footer;