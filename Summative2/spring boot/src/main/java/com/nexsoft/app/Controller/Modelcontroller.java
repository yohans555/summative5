package com.nexsoft.app.Controller;

import java.util.ArrayList;

import com.nexsoft.app.model.Image;
import com.nexsoft.app.model.Section1;
import com.nexsoft.app.service.Imageservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Modelcontroller {


    @Autowired
    Imageservice imageservice;

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value ="/get/image", produces = "application/json")
    public ArrayList<Image> getData(){
        return imageservice.get();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value ="/get/section", produces = "application/json")
    public ArrayList<Section1> getSection(){
        return imageservice.getSect();
    }

 
}
