package com.nexsoft.app.service;

import java.util.ArrayList;
import javax.transaction.Transactional;

import com.nexsoft.app.model.Image;
import com.nexsoft.app.model.Section1;
import com.nexsoft.app.repo.Imagerepository;
import com.nexsoft.app.repo.Section1repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Imageservice {
    @Autowired
     private Imagerepository productrepository;

     @Autowired
     private Section1repository section1repository;

    @Transactional
    public ArrayList<Image> get(){
        return (ArrayList<Image>) productrepository.findAll();
    }

    @Transactional
    public ArrayList<Section1> getSect(){
        return (ArrayList<Section1>) section1repository.findAll();
    }
}
