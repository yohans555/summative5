package com.nexsoft.app.repo;


import com.nexsoft.app.model.Section1;

import org.springframework.data.jpa.repository.JpaRepository;


public interface Section1repository extends JpaRepository<Section1, Integer> {
}


